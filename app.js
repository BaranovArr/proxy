const express = require('express');
const { createProxyMiddleware, responseInterceptor } = require('http-proxy-middleware');
const fetch = require('node-fetch');

const app = express();

const proxyMiddleware = createProxyMiddleware({
  target: "https://jsonplaceholder.typicode.com/posts/1",
  changeOrigin: true,
  secure: false,
  selfHandleResponse: true, // важно, без этой опции responseInterceptor не будет отрабатывать
  on: {
    proxyRes: responseInterceptor(async (responseBuffer, proxyRes, req, res) => {
      const response = responseBuffer.toString('utf8');
      return response;
    }),
    error: (err, req, res) => {
      console.log("Error");
      console.log(err);
    }
  },
});

if(process.env.USE_PROXY === 'true') {
  app.use('/', proxyMiddleware);
}
app.get('/', async (req, res) => {
  try {
    // make a fetch request to an external API
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/1');
    const json = await response.json();

    res.send(json); // send the response back to the client
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});
